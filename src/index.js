
import express from 'express'
import cors from 'cors'
import socket from 'socket.io'
import bodyParser from 'body-parser'
import tmi from 'tmi.js'
import * as data from './data'
import { bookshelf, models, Segment, Actor } from './db/db'
import { rando } from './utils/general'

import fs from 'fs'
import osc from 'osc'
import https from 'https'
const port = 3000

var key = fs.readFileSync(__dirname + '/encryption/private.key')
var cert = fs.readFileSync(__dirname + '/encryption/primary.crt')
var ca = fs.readFileSync(__dirname + '/encryption/intermediate.crt')

var sslOptions = {
  key: key,
  cert: cert,
  ca: ca,
}

const app = express() 
const http = require('http').Server(app)
const io = socket(http)

let clientID = 'y6a8gwhfwuqvj6r9nriyhxe3c80fv5'
let secret = 't9febhye6u5j9dmzde2hi87muirblv'

let oAuth = {
  access_token: "oauth:ofbqsg2ri38g9wl6xaig05v92ia0tz",
  expires_in: 5326521,
  scope: [
      "chat:read"
  ],
  token_type: "bearer"
}

const opts = {
  identity: {
    username: 'eli7vh',
    password: oAuth.access_token
  },
  channels: [
    'vapsquad',
    'eli7vh'
  ]
};

const client = new tmi.client(opts)

var state = {
  chat: '',
  chat_log: [],
  actors: [],
  in_booth: [],
  segments: [],
  segment: {},
  // randomized data
  accents: null,
  adjectives: null,
  nouns: null,
  verbs: null,
  countries: null,

}

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.options('*', cors())

// INITIAL STATE
// POST 

app.post('/chat', (req, res) => {
  let payload = req.body.message
  state.chat = payload
  state.chat_log.push(payload)
  io.sockets.emit('chat', payload)
  res.send("chat submitted")
})

app.post('/segment', (req, res) => {
  let payload = req.body
  state.segment = payload
  io.sockets.emit('segment',  payload)
  res.send(true)
})

app.post('/in_booth', (req, res) => {
  let payload = req.body
  state.in_booth = payload
  io.sockets.emit('in_booth',  payload)
  console.log("booth updated!", payload)
  // update each actor's "times_in_booth" after updating sockets
  res.send(true)
})

// GET COLLECTIONS
app.get('/state', (req,res) => {
  res.send(state)
})

app.get('/indexes/random', (req,res) => {
  res.json(Object.keys(data))
})

app.get('/:model', (req, res) => {
  models[req.params.model].fetchAll().then(data => {
    res.json(data)
  })
})

app.get('/data/:thing', (req,res) => {
  res.json(data[req.params.thing])
})

app.get('/random/:thing', (req,res) => {
  let thing = req.param.thing
  let random_pick = rando(data[req.params.thing])
  state[thing] = random_pick
  res.send(random_pick)
})

// socket shit

io.origins('*:*')

io.on('connection', (socket) => {
  console.log("a user connected - ID:", socket.id)
  io.sockets.emit('in_booth', state.in_booth)
  io.sockets.emit('segment', state.segment)
  io.sockets.emit()
  socket.on('disconnect', () => {
    console.log('user has disconnected')
  })
})

client.on('ping', (target, context, msg, self)=>{
})

client.on('disconnect', function () {
  console.log( 'disconnected from twitch IRC!!' );
});

client.on('message', (target, context, msg, self)=>{
  console.log("chat recieved =>", msg)
  io.sockets.emit('chat', {
    target,
    context,
    msg,
  })
})
client.connect()

http.listen(port, () => {
  console.log("APP IS RUNNING =>", port)
})