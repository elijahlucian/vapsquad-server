import actors from './data/actors'
import accents from './data/accents'
import adjectives from './data/adjectives'
import animals from './data/animals'
import countries from './data/countries'
import emotions from './data/emotions'
import nouns from './data/nouns'
import segments from './data/segments'
import verbs from './data/verbs'
import voices from './data/voices'
import voiceStyles from './data/voiceStyles'

export {
  accents,
  actors,
  adjectives,
  animals,
  countries,
  emotions,
  nouns,
  segments,
  verbs,
  voices,
  voiceStyles,
}

// MORE WORDS AND SHIT
// https://www.enchantedlearning.com/wordlist/