import random from 'random'

const rando = (array) => {
  return array[random.int(0,array.length - 1)]
}

const capitalize = (word) => {
  return word.charAt(0).toUpperCase() + word.slice(1)
}

const range = (limit) => {
  return [...Array(limit).keys()]
}

export { rando, capitalize, range }