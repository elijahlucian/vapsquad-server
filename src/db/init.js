import { actors, segments, accents, countries } from '../data'

const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: './src/db/vapsquad.sqlite'
  },
  useNullAsDefault: true,
})

const bookshelf = require('bookshelf')(knex)
const db = bookshelf.knex.schema

// VAPSQUAD RELATED

db.dropTableIfExists('actors')
.createTable('actors', t => {
  t.increments('id')
  t.string('name')
  t.string('handle')
  t.string('tagline')
  t.string('color')
  t.integer('times_in_booth')
  t.timestamps()
})
.then(() => {
  console.log("importing actors", actors.length)
  actors.forEach(actor => {
    let {name, handle, color, tagline} = actor
    bookshelf.knex('actors').insert({
      name,
      color,
      handle,
      tagline,
      created_at: Date.now(),
      updated_at: Date.now(),
    })
    .then(res => console.log("inserted actor", res))
  })
  console.log("Actors Table Created and seeded!")
})

db.dropTableIfExists('projects')
.createTable('projects', t => {
  t.increments('id')
  t.string('name')
  t.string('segment')
  t.string('content_creator')
})
.then(() => {
  console.log("Projects Table Created!")
})

// STREAM RELATED

db.dropTableIfExists('segments')
.createTable('segments', t => {
  t.increments('id')
  t.string('name')
})
.then(() => {
  console.log("importing segments", segments.length)
  segments.forEach(segment => {
    let {name} = segment
    bookshelf.knex('segments').insert({name})
    .then(res => console.log("inserted segment", res))
  })
  console.log("Segments Table Created and seeded!")
})

db.dropTableIfExists('chats')
.createTable('chats', t => {
  t.increments('id')
  t.string('user')
  t.string('message')
  t.string('channel')
  t.timestamps()
})
.then(() => {
  console.log("Chats Table Created!")
})

db.dropTableIfExists('users')
.createTable('users', t => {
  t.increments('id')
  t.string('username')
  t.integer('level')
  t.integer('role').default(0)
  t.timestamps()
})
.then(() => {
  console.log("Users Table Created!")
})

// RANDOMIZER DATA

