const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: './src/db/vapsquad.sqlite'
  }
})

const bookshelf = require('bookshelf')(knex)

const Segment = bookshelf.Model.extend({
  tableName: 'segments',
})

const Actor = bookshelf.Model.extend({
  tableName: 'actors',
})

const models = {
  actors: Actor,
  segments: Segment,
}

export { 
  bookshelf,
  models,
  Segment,
  Actor,
}