export default [
  {
    id: 1,
    name: "elijah",
    handle: "@eli7vh",
    tagline: "The Diggity Dankest",
    color: "#ff69b4"
  },
  {
    id: 2,
    name: "shylo",
    handle: "@ltsnakeplissken",
    tagline: "Definitely exists and is alive",
    color: "#000080"
  },
  {
    id: 3,
    name: "nat",
    handle: "@flippychips",
    tagline: "Fellow of infinite jest",
    color: "#df5286"
  },
  {
    id: 4,
    name: "kaela",
    handle: "@missyjopinup",
    tagline: "She Preddy",
    color: "#008081"
  },
  {
    id: 5,
    name: "tarence",
    handle: "@tedwards",
    tagline: "I once met the Queen",
    color: "#516e4d"
  },
  {
    id: 6,
    name: "dawn",
    handle: "@vader_bad_dawn_good",
    tagline: "colorful, friendly, happy",
    color: "#ff0000"
  },
  {
    id: 7,
    name: "nancy",
    handle: "@exdevlin",
    tagline: "Unassuming tiny rage",
    color: "#771f1f"
  },
  {
    id: 8,
    name: "eric",
    handle: "@cytokinesis",
    tagline: "Does he have free time?!",
    color: "#fe5a1d"
  },
  {
    id: 9,
    name: "randolph",
    handle: "@west_yyc",
    tagline: "Officially disguised as a Canadian",
    color: "#80a4ed"
  }
];
